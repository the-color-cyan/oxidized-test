! show version
! Cisco Nexus Operating System (NX-OS) Software
! TAC support: http://www.cisco.com/tac
! Copyright (C) 2002-2015, Cisco and/or its affiliates.
! All rights reserved.
! The copyrights to certain works contained in this software are
! owned by other third parties and used and distributed under their own
! licenses, such as open source.  This software is provided "as is," and unless
! otherwise stated, there is no warranty, express or implied, including but not
! limited to warranties of merchantability and fitness for a particular purpose.
! Certain components of this software are licensed under
! the GNU General Public License (GPL) version 2.0 or 
! GNU General Public License (GPL) version 3.0  or the GNU
! Lesser General Public License (LGPL) Version 2.1 or 
! Lesser General Public License (LGPL) Version 2.0. 
! A copy of each such license is available at
! http://www.opensource.org/licenses/gpl-2.0.php and
! http://opensource.org/licenses/gpl-3.0.html and
! http://www.opensource.org/licenses/lgpl-2.1.php and
! http://www.gnu.org/licenses/old-licenses/library.txt.
! 
! Software
!   BIOS: version 07.17
!   NXOS: version 7.0(3)I1(2)
!   BIOS compile time:  09/10/2014
!   NXOS image file is: bootflash:///n9000-dk9.7.0.3.I1.2.bin
!   NXOS compile time:  5/16/2015 12:00:00 [05/16/2015 19:07:58]
! 
! 
! Hardware
!   cisco Nexus9000 C9396PX Chassis 
!   Intel(R) Core(TM) i3-3227U C with 16402540 kB of memory.
!   Processor Board ID SAL1829X4XX
! 
!   Device name: ABS02AXSCCC
!   bootflash:    7906304 kB
! show inventory
! NAME: "Chassis",  DESCR: "Nexus9000 C9396PX Chassis"             
! PID: N9K-C9396PX         ,  VID: V02 ,  SN: SAL1829X4XX          
! 
! NAME: "Slot 1",  DESCR: "1/10G SFP+ Ethernet Module"            
! PID: N9K-C9396PX         ,  VID: V02 ,  SN: SAL1829X4XX          
! 
! NAME: "Slot 2",  DESCR: "40G Ethernet Expansion Module"         
! PID: N9K-M12PQ           ,  VID: V01 ,  SN: SAL1821T68J          
! 
! NAME: "Power Supply 1",  DESCR: "Nexus9000 C9396PX Chassis Power Supply"
! PID: N9K-PAC-650W-B      ,  VID: V01 ,  SN: DCB1826Y0D3          
! 
! NAME: "Power Supply 2",  DESCR: "Nexus9000 C9396PX Chassis Power Supply"
! PID: N9K-PAC-650W-B      ,  VID: V01 ,  SN: DCB1826Y0D6          
! 
! NAME: "Fan 1",  DESCR: "Nexus9000 C9396PX Chassis Fan Module"  
! PID: N9K-C9300-FAN2-B    ,  VID: V01 ,  SN: N/A                  
! 
! NAME: "Fan 2",  DESCR: "Nexus9000 C9396PX Chassis Fan Module"  
! PID: N9K-C9300-FAN2-B    ,  VID: V01 ,  SN: N/A                  
! 
! NAME: "Fan 3",  DESCR: "Nexus9000 C9396PX Chassis Fan Module"  
! PID: N9K-C9300-FAN2-B    ,  VID: V01 ,  SN: N/A                  
! 
! 
! show running-config

!Command: show running-config

version 7.0(3)I1(2)
hostname ABS02AXSCCC
vdc ABS02AXSCCC id 1
  limit-resource vlan minimum 16 maximum 4094
  limit-resource vrf minimum 2 maximum 4096
  limit-resource port-channel minimum 0 maximum 511
  limit-resource u4route-mem minimum 248 maximum 248
  limit-resource u6route-mem minimum 96 maximum 96
  limit-resource m4route-mem minimum 58 maximum 58
  limit-resource m6route-mem minimum 8 maximum 8

feature telnet
feature nxapi
feature scp-server
feature tacacs+
cfs eth distribute
feature ospf
feature interface-vlan
feature hsrp
feature lacp
feature vpc
feature lldp
feature vtp

no password strength-check
username admin password 5 $1$QONQUpLT$kOZIoEjAeTvd8aRUMyaDy1  role network-admin
username acenapalm password 5 $1$tjCSIyyC$0I9.Rz1EMX/dvbVARrH.p0  role network-admin
username acelab password 5 $1$d8IFdTpk$e1ozMxOeWrQJB.ety9chJ/  role network-admin

banner motd ^
***** WARNING  ***************************************************************
* This system is  restricted  solely  to  authorized  users  for  legitimate *
* business  purposes  only.  The actual or  attempted  unauthorized  access, *
* use or modification of this system is strictly prohibited.   You  have  no *
* expectation of privacy  in  its  use.   To  ensure  that  the  system   is *
* functioning properly, individuals using this computer system  are  subject *
* to having all  of  their  activities  monitored  and  recorded  by  system *
* personnel.  Use of  this  system  evidences an  express  consent  to  such *
* monitoring and agreement that  if  such  monitoring  reveals  evidence  of *
* possible abuse or  criminal  activity,  system  personnel  may provide the *
* results of such  monitoring  to  appropriate  law  enforcement  officials. *
* Unauthorized users are subject to company disciplinary proceedings  and/or *
* criminal and civil penalties under  state,  federal  or  other  applicable *
* foreign and domestic laws.                                                 *
******************************************************************************
^

no ip domain-lookup
tacacs-server key 7 "FdyrtacBmplfx!"
ip tacacs source-interface Vlan2255
tacacs-server host 192.168.255.1 
aaa group server tacacs+ tacacs+ 
    server 192.168.255.1 
spanning-tree mode mst
copp profile strict
snmp-server user admin network-admin auth md5 0x05c423e06fcfa9561c724dc7d70c5f0e priv 0x05c423e06fcfa9561c724dc7d70c5f0e localizedkey
snmp-server user acelab network-admin auth md5 0x59a1d4ee53ba5f568a6421d4badd63a5 priv 0x59a1d4ee53ba5f568a6421d4badd63a5 localizedkey
snmp-server user acenapalm network-admin auth md5 0xa3aa6c9ab7f79123b4ddab5b417406e4 priv 0xa3aa6c9ab7f79123b4ddab5b417406e4 localizedkey
rmon event 1 log trap public description FATAL(1) owner PMON@FATAL
rmon event 2 log trap public description CRITICAL(2) owner PMON@CRITICAL
rmon event 3 log trap public description ERROR(3) owner PMON@ERROR
rmon event 4 log trap public description WARNING(4) owner PMON@WARNING
rmon event 5 log trap public description INFORMATION(5) owner PMON@INFO
aaa authentication login default group tacacs+ 
aaa authorization ssh-certificate default group tacacs+ 
aaa authorization config-commands default group tacacs+ local 
aaa authorization commands default group tacacs+ local 
aaa accounting default group tacacs+ 

ip route 0.0.0.0/0 192.168.255.254
vlan 1,1101,2000,2100,2250-2255,3101,3966-3967
vlan 1101
  name HIGH-SEC-SRVR-BASTION-GREEN-NET
vlan 2000
  name EMERGING-NET-ENT-DATA-PROD
vlan 2100
  name EVE-NG-L3-SUBNET
vlan 2250
  name DMZ-BACKBONE-NET
vlan 2251
  name LAN-BJ-SERVER-IMPI
vlan 2252
  name LAN-BG-SERVER-IMPI
vlan 2253
  name LAN-AJ-SERVER-IMPI
vlan 2254
  name DMZ-UNTRUSTED-IGP
vlan 2255
  name DMZ-TRUSTED-IGP
vlan 3101
  name SHARED-SERVICES-OOB-NET
vlan 3966
  name INFRA-P2P-ABR-CORE-RTRS
vlan 3967
  name INFRA-SS-APOC-ATTACHMENT

vrf context management

interface Vlan1

interface Vlan2255
  description DMZ-IGP
  no shutdown
  ip address 192.168.255.9/24

interface port-channel1
  description ABS01AXSCCC::ABS02AXSCCC(Po1)
  switchport mode trunk
  switchport trunk allowed vlan 1101,2000,2100,2250-2255,3101
  mtu 9216

interface Ethernet1/1
  description HIGH-SEC-DMZ-SRVR(PORT-2)(BJ.R02.R07.RU15)(CKTID-TBD)
  switchport mode trunk
  switchport trunk allowed vlan 1101,2254-2255
  mtu 9216

interface Ethernet1/2
  description LOW-SEC-DMZ-SRVR-001(PORT-2)(BJ.R02.R07.RU14)(CKTID-TBD)
  switchport mode trunk
  switchport trunk allowed vlan 2255
  mtu 9216

interface Ethernet1/3
  description LOW-SEC-DMZ-SRVR-002(PORT-2)(BJ.R02.R07.RU12)(CKTID-TBD)
  switchport mode trunk
  switchport trunk allowed vlan 2255,3101
  mtu 9216

interface Ethernet1/4
  shutdown

interface Ethernet1/5
  shutdown

interface Ethernet1/6
  shutdown

interface Ethernet1/7
  shutdown

interface Ethernet1/8
  shutdown

interface Ethernet1/9
  shutdown

interface Ethernet1/10
  shutdown

interface Ethernet1/11
  shutdown

interface Ethernet1/12
  shutdown

interface Ethernet1/13
  shutdown

interface Ethernet1/14
  shutdown

interface Ethernet1/15
  shutdown

interface Ethernet1/16

interface Ethernet1/17
  shutdown

interface Ethernet1/18
  shutdown

interface Ethernet1/19
  shutdown

interface Ethernet1/20
  shutdown

interface Ethernet1/21
  shutdown

interface Ethernet1/22
  shutdown

interface Ethernet1/23
  shutdown

interface Ethernet1/24
  shutdown

interface Ethernet1/25
  shutdown

interface Ethernet1/26
  shutdown

interface Ethernet1/27
  shutdown

interface Ethernet1/28
  shutdown

interface Ethernet1/29
  shutdown

interface Ethernet1/30
  shutdown

interface Ethernet1/31
  shutdown

interface Ethernet1/32
  shutdown

interface Ethernet1/33
  shutdown

interface Ethernet1/34
  shutdown

interface Ethernet1/35
  shutdown

interface Ethernet1/36
  shutdown

interface Ethernet1/37
  shutdown

interface Ethernet1/38
  shutdown

interface Ethernet1/39
  shutdown

interface Ethernet1/40
  shutdown

interface Ethernet1/41
  shutdown

interface Ethernet1/42
  shutdown

interface Ethernet1/43
  description P2P::AMS02AXSCCC::Te1/52
  switchport mode trunk
  switchport trunk allowed vlan 2253,2255,3101
  mtu 9216

interface Ethernet1/44

interface Ethernet1/45
  shutdown

interface Ethernet1/46
  description ABF02AXSCCC(xe-10/0/1)(BJ.02.R07.RU03)(CKTID-TBD)
  switchport mode trunk
  switchport trunk allowed vlan 1101,2000,2100,2250-2255
  mtu 9216

interface Ethernet1/47
  description ABF02AXSCCC(xe-10/0/0)(BJ.R02.R07.RU03)(CKTID-TBD)
  switchport access vlan 3967
  mtu 9216

interface Ethernet1/48
  description SAR02ZCSCCC(Te0/0/0/15)(AH.R01.R05.RU10)(CKTID-2-8563)
  switchport access vlan 3967
  mtu 9216

interface Ethernet2/1
  description ABS01AXSCCC(Eth2/1)(BJ.R02.R07.RU10)(CKTID-TBD)
  switchport mode trunk
  switchport trunk allowed vlan 1101,2000,2100,2250-2255,3101
  mtu 9216
  channel-group 1

interface Ethernet2/2
  description ABS01AXSCCC(Eth2/2)(BJ.R02.R07.RU10)(CKTID-TBD)
  switchport mode trunk
  switchport trunk allowed vlan 1101,2000,2100,2250-2255,3101
  mtu 9216
  channel-group 1

interface Ethernet2/3

interface Ethernet2/4
  shutdown

interface Ethernet2/5

interface Ethernet2/6
  shutdown

interface Ethernet2/7
  shutdown

interface Ethernet2/8
  shutdown

interface Ethernet2/9
  shutdown

interface Ethernet2/10
  description P2P::ACE-TEST-SRVR-001::PCIe1-B
  switchport mode trunk
  switchport trunk allowed vlan 2255,3101

interface Ethernet2/11
  shutdown

interface Ethernet2/12
  shutdown

interface mgmt0
  vrf member management
line console
  exec-timeout 10
  terminal width  160
line vty
boot nxos bootflash:/n9000-dk9.7.0.3.I1.2.bin 



